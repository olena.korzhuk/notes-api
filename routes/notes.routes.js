const {Router} = require('express');

const notesController = require('./../controllers/notes.controller');

const router = Router();

router.get('/', notesController.geNotes);
router.get('/:id', notesController.getNote);
router.put('/:id', notesController.updateNote);
router.patch('/:id', notesController.toggleCompleted);
router.delete('/:id', notesController.deleteNote);
router.post('/', notesController.createNote);

module.exports = router;