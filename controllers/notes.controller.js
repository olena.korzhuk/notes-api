const Note = require('./../models/Note');
const User = require('./../models/User');

module.exports.createNote = async (req, res) => {
    const {id} = req.user;
    const {text} = req.body;

    if (!text) {
        return res.status(400).json({
            message: 'You should provide text for note'
        });
    }

    try {
        const user = await User.findOne({_id: id})
        const newNote = {
            userId: user._id,
            text,
            createdDate: new Date().toISOString()
        }
        try {
            await Note.create(newNote);
            res.status(200).json({message: 'Success'});
        } catch (e) {
            console.error('CREATE NOTE ERROR', e)
        }

    } catch (e) {
        return res.status(500).json({message: 'Create Note: Internal server error'});
    }
};

module.exports.geNotes = async (req, res) => {
    const {id} = req.user;
    const offset = parseInt(req.query.offset) || 0;
    const limit = parseInt(req.query.limit) || 0;

    try {
        const notesCount = (await Note.find({userId: id})).length

        const notes = await Note.find({userId: id})
            .select('-__v')
            .skip(offset)
            .limit(limit)
            .exec();

        res.status(200).json({
            offset,
            limit,
            notesCount,
            notes
        });

    } catch (e) {
        return res.status(500).json({message: 'Create Note: Internal server error'});
    }
};

module.exports.getNote = async (req, res) => {
    const noteId = req.params.id;
    const {id} = req.user;

    try {
        const note = await Note.findOne({userId: id, _id: noteId})
            .select('-__v');

        if (note) {
            res.status(200).json({
                note
            });
        } else {
            return res.status(500).json({message: 'Note not found'});
        }
    } catch (e) {
        return res.status(500).json({message: 'Create Note: Internal server error'});
    }
}

module.exports.updateNote = async (req, res) => {
    const noteId = req.params.id;
    const {text} = req.body;
    const {id} = req.user;

    if (!noteId) {
        return res.status(400).json({
            message: 'You should specify Id od the note'
        });
    }

    try {
        const note = await Note.findOne({userId: id, _id: noteId});
        if (note) {
            await Note.findOneAndUpdate({_id: noteId}, {text: text})
                .select('-__v');

            res.status(200).json({
                message: 'Note updated successfully'
            });
        } else {
            return res.status(500).json({message: 'No note found'});
        }
    } catch (e) {
        return res.status(500).json({message: 'Internal server error'});
    }
};

module.exports.toggleCompleted = async (req, res) => {
    const noteId = req.params.id;
    const {id} = req.user;

    if (!noteId) {
        return res.status(400).json({
            message: 'You should specify Id of the note'
        });
    }

    try {
        const note = await Note.findOne({userId: id, _id: noteId})
            .select('-__v');

        note.completed = !note.completed;
        await note.save();

        res.status(200).json({
            message: 'Note status changed'
        });

    } catch (e) {
        return res.status(500).json({message: 'Toggle note status: Internal server error'});
    }
};

module.exports.deleteNote = async (req, res) => {
    const noteId = req.params.id;
    const {id} = req.user;

    if (!noteId) {
        return res.status(400).json({
            message: 'You should specify Id of the note'
        });
    }

    try {
        const note = await Note.findOne({userId: id, _id: noteId})
            .select('-__v');
        if (note) {
            await Note.deleteOne({_id: noteId});
            res.status(200).json({message: 'Success'});
        } else {
            return res.status(500).json({message: 'Note not found'});
        }
    } catch (e) {
        return res.status(500).json({message: 'Internal server error'});
    }
};