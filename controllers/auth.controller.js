const User = require('./../models/User');
const bcrypt = require('bcryptjs');
const tokenService = require('./../services/token.service');

module.exports.register = async (req, res) => {
    const {username, password} = req.body;

    if (!username) {
        res.status(400).json({
            message: `Please enter username`
        });
        return
    }

    if (!password) {
        res.status(400).json({
            message: `Please enter password`
        });
        return
    }

    try {
        const sameUser = await User.findOne({username});
        if (sameUser) {
            return res.status(400).json({
                message: 'User with the same username already exists'
            });
        } else {
            try {
                await User.create({
                    username,
                    password,
                    createdDate: new Date().toISOString()
                });
                res.status(200).json({
                    message: 'Success'
                });
            } catch (e) {
                console.log('REGISTER ERROR', e);
            }
        }
    } catch (e) {
        return res.status(500).send({
            message: 'Internal Server Error'
        });
    }
};

module.exports.login = async (req, res) => {
    const {username, password} = req.body;

    if (!username) {
        res.status(400).json({
            message: `Please enter username`
        });
        return
    }

    if (!password) {
        res.status(400).json({
            message: `Please enter password`
        });
        return
    }

    try {
        const user = await User.findOne({username});
        if (!user) {
            return res.status(400).json({
                message: 'Incorrect username'
            });
        } else {
            const isValidPassword = await bcrypt.compare(password, user.password);

            if (!isValidPassword) {
                return res.status(400).json({
                    message: 'Incorrect password or username'
                });
            }

            const token = tokenService.generateToken({name: user.username, id: user._id});
            res.status(200).json({
                message: 'Success',
                jwt_token: token
            });
        }
    } catch (e) {
        return res.status(500).send({
            message: 'Internal Server Error'
        });
    }
};