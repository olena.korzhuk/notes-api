const User = require('./../models/User');

module.exports.getUserInfo = async (req, res) => {
    const {id} = req.user;

    try {
        const user = await User.findOne({_id: id})
        const response = {
            _id: user._id,
            username: user.username,
            createdDate: user.createdDate
        }
        res.status(200).json({
            user: response
        });
    } catch (e) {
        return res.status(500).json({message: 'Internal server error'});
    }
};

module.exports.deleteUserProfile = async (req, res) => {
    const {id} = req.user;

    try {
        const user = await User.findOne({_id: id});
        if (user) {
            await User.deleteOne({_id: user._id});
            res.status(200).json({message: 'Success'});
        } else {
            return res.status(500).json({message: 'User not found'});
        }
    } catch (e) {
        return res.status(500).json({message: 'Internal server error'});
    }
};

module.exports.changePassword = async (req, res) => {
    const {id} = req.user;
    const {newPassword} = req.body;

    try {
        let user = await User.findOne({_id: id});
        if (user) {
            user.password = newPassword;
            await user.save();
            res.status(200).json({message: 'Success'});
        } else {
            return res.status(500).json({message: 'User not found'});
        }
    } catch (e) {
        return res.status(500).json({message: 'Internal server error'});
    }
};