const express = require('express');
const mongoose = require('mongoose');
const morgan = require('morgan');
const cors = require('cors');
const app = express();
const authRoutes = require('./routes/auth.routes');
const usersRoutes = require('./routes/users.routes');
const notesRoutes = require('./routes/notes.routes');
require('dotenv').config();
const port = process.env.PORT || 8080;
const uri = process.env.DB_URI;
const {authVerifyToken} = require('./middleware/auth.middleware');

app.use(express.urlencoded({extended: false}));
app.use(express.json());
// app.use(cors);
app.use(morgan('tiny'));

const connectToDb = async (uri) => {
    try {
        await mongoose.connect(uri);
        console.log('CONNECTED TO MONGO')
    } catch (e) {
        console.error('Failed to connect to MongoDB', e);
    }
}

app.use('/api/auth', authRoutes);
app.use('/api/users', authVerifyToken, usersRoutes);
app.use('/api/notes', authVerifyToken, notesRoutes);

connectToDb(uri)
    .then(() => app.listen(port, () => console.log(`Server is up and running on port ${port}`)));
