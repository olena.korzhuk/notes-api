const jwt = require('jsonwebtoken');
const separator = ' ';

module.exports.authVerifyToken = (req, res, next) => {
    const token = req.headers.authorization;

    if (!token) {
        return res.status(400).json({message: 'Access token is not provided for the request'});
    }

    const tokenSplit = token.split(separator);
    const accessToken = tokenSplit[tokenSplit.length - 1];
    try {
        const verified = jwt.verify(accessToken, process.env.JWT_SECRET);
        req.user = {id: verified.id, name: verified.name};
        next();
    } catch (e) {
        return res.status(400).json({message: 'Invalid access token'});
    }
};